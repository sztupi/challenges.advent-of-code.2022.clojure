(ns day5
  "playground for aoc2022 day5"

  (:require [clojure.string :as str]
            [blancas.kern.core :as k]))

(def crate-or-empty-p
  (k/<|> (k/>> (k/times 3 k/space) (k/return :empty))
         (k/>> (k/sym* \[)
               (k/<< k/any-char (k/sym* \])))))

(def crate-line-p "parses a single crate line"
  (k/sep-by1 k/space crate-or-empty-p))

(def move-p "parses single move line"
  (k/bind [_ (k/token* "move ")
           count k/dec-num
           _ (k/token* " from ")
           from k/dec-num
           _ (k/token* " to ")
           to k/dec-num]
          (k/return {:count count :from from :to to})))

(def moves-p "parses moves section"
  (k/sep-by1 k/new-line* move-p))

(def crates-header-p "parses column heads"
  (k/sep-by1 k/space
             (k/<$> #(Integer. (str/trim (str/join %)))
                    (k/times 3 k/any-char))))

(def crates-p "parses crate section"
  (k/bind [crate-lines (k/sep-end-by1 k/new-line* (k/<:> crate-line-p))
           crate-headers crates-header-p]
          (let* [transposed-lines (apply mapv vector crate-lines)
                 final-lines (map (fn [line] (reverse (filter #(not (= :empty %)) line)))
                                  transposed-lines)]
            (k/return (zipmap crate-headers final-lines)))))

(def input-p "parses entire input"
  (k/bind [crates crates-p
           _ (k/times 2 k/new-line*)
           moves moves-p]
          (k/return {:crates crates :moves moves})))

(defn -main
  "Invoke me with clojure -M -m scratch"
  [& args]
  (let [input (str/join "\n" ["    [D]    "
                              "[N] [C]    "
                              "[Z] [M] [P]"
                              " 1   2   3 "
                              ""
                              "move 1 from 2 to 1"
                              "move 3 from 1 to 3"
                              "move 2 from 2 to 1"
                              "move 1 from 1 to 2"])]

    (print (k/parse input-p input))
    ))
