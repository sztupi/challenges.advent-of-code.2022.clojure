(ns day5-test
  (:require [clojure.test :refer :all]
            [day5 :refer :all]
            [blancas.kern.core :as k]
            [clojure.string :as str]))

(deftest crate-line-parsing
  (is (= (k/value crate-line-p  "    [D]    ")
         [:empty \D :empty])))

(deftest crates-header-parsing
  (is (= (k/value crates-header-p  " 1   2   3 ")
         [1 2 3])))

(deftest crate-section-parsing
  (let [input (str/join "\n" ["    [D]    "
                              "[N] [C]    "
                              "[Z] [M] [P]"
                              " 1   2   3 "])]
    (is (= (k/value crates-p input)
           {1 [\Z \N]
            2 [\M \C \D]
            3 [\P]}
           ))))

(deftest move-parsing
  (is (= (k/value move-p "move 3 from 1 to 3")
         {:count 3 :from 1 :to 3})))

(deftest moves-parsing
  (let [input (str/join "\n" ["move 1 from 2 to 1"
                              "move 3 from 1 to 3"
                              "move 2 from 2 to 1"
                              "move 1 from 1 to 2"])]
    (is (= (k/value moves-p input)
           [{:count 1 :from 2 :to 1}
            {:count 3 :from 1 :to 3}
            {:count 2 :from 2 :to 1}
            {:count 1 :from 1 :to 2}
            ]))))

(deftest input-parsing
  (let [input (str/join "\n" ["    [D]    "
                              "[N] [C]    "
                              "[Z] [M] [P]"
                              " 1   2   3 "
                              ""
                              "move 1 from 2 to 1"
                              "move 3 from 1 to 3"
                              "move 2 from 2 to 1"
                              "move 1 from 1 to 2"])]
    (is (= (k/value input-p input)
           {:crates
            {1 [\Z \N]
             2 [\M \C \D]
             3 [\P]}
            :moves
            [{:count 1 :from 2 :to 1}
             {:count 3 :from 1 :to 3}
             {:count 2 :from 2 :to 1}
             {:count 1 :from 1 :to 2}
             ]}))))
